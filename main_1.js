const questions = [
	{
		question: "Какой язык работает в браузере?",
		answers: ["Java", "C", "Python", "JavaScript"],
		correct: 4,
	},
	{
		question: "Что означает CSS?",
		answers: [
			"Central Style Sheets",
			"Cascading Style Sheets",
			"Cascading Simple Sheets",
			"Cars SUVs Sailboats",
		],
		correct: 2,
	},
	{
		question: "Что означает HTML?",
		answers: [
			"Hypertext Markup Language",
			"Hypertext Markdown Language",
			"Hyperloop Machine Language",
			"Helicopters Terminals Motorboats Lamborginis",
		],
		correct: 1,
	},
	{
		question: "В каком году был создан JavaScript?",
		answers: ["1996", "1995", "1994", "все ответы неверные"],
		correct: 2,
	},
];

// Находим элементы
const headerContainer = document.querySelector('#header');
const listContainer = document.querySelector('#list');
const submitBtn = document.querySelector('#submit');


//Переменные игры
let score = 0; // кол-во правильных ответов
let questionIndex = 0; // текущий вопрос

clearPage()
showQuestion()
submitBtn.onclick = checkAnswer

function clearPage(){
	headerContainer.innerHTML = ''
	listContainer.innerHTML = ''
}

function showQuestion(){
	const title = `<h2 class="title">${questions[questionIndex].question}</h2>`
	headerContainer.innerHTML = title


	let answerNumber = 1
	// варианты ответов
	for(answerText of questions[questionIndex]['answers']){
		// console.log(answerNumber, answerText)
		let answerHtml = `
		<li>
			<label>
					<input value="${answerNumber}" type="radio" class="answer" name="answer" /><!--в value будем вставлять номер ответа, который пользователь выбрал-->
					<span>${answerText}</span>
			</label>
		</li>
		`;
		console.log(answerHtml)
		answerHtml = answerHtml.replace('%number%', answerNumber)
		listContainer.innerHTML += answerHtml
		answerNumber++
	}
 }

 function checkAnswer(){
	const checkedRadio = listContainer.querySelector('input[type="radio"]:checked')
	// console.log(checkedRadio)
	 /*
	 if(checkedRadio){
		 console.log('OK')
	 }else{
		submitBtn.blur()
		return
	 }
	  */
	 if(!checkedRadio){
		 submitBtn.blur()
		 return
	 }
 }



