const questions = [
	{
		question: "Какой язык работает в браузере?",
		answers: ["Java", "C", "Python", "JavaScript"],
		correct: 4,
	},
	{
		question: "Что означает CSS?",
		answers: [
			"Central Style Sheets",
			"Cascading Style Sheets",
			"Cascading Simple Sheets",
			"Cars SUVs Sailboats",
		],
		correct: 2,
	},
	{
		question: "Что означает HTML?",
		answers: [
			"Hypertext Markup Language",
			"Hypertext Markdown Language",
			"Hyperloop Machine Language",
			"Helicopters Terminals Motorboats Lamborginis",
		],
		correct: 1,
	},
	{
		question: "В каком году был создан JavaScript?",
		answers: ["1996", "1995", "1994", "все ответы неверные"],
		correct: 2,
	},
];

// Находим элементы
const headerContainer = document.querySelector('#header');
const listContainer = document.querySelector('#list');
const submitBtn = document.querySelector('#submit');


//Переменные игры
let score = 0; // кол-во правильных ответов
let questionIndex = 0; // текущий вопрос

clearPage()
showQuestion()
submitBtn.onclick = checkAnswer

function clearPage(){
	headerContainer.innerHTML = ''
	listContainer.innerHTML = ''
}

function showQuestion(){
	// console.log(questions[questionIndex].question)
	// console.log(questions[questionIndex]['question']) // question - строка, т.е. свойство в объекте
	//
	//
	// console.log(questions[questionIndex]['answers']) // варианты ответов

	// const headerTemplate = `<h2 class="title">%title%</h2>` // здесь применим шаблонную метку
	// const title = headerTemplate.replace('%title%', questions[questionIndex].question) // сделает замену подстроки внутри строки, т.е. мы строку %title% заменяем на questions[questionIndex].question
	// console.log(headerTemplate)

	const title = `<h2 class="title">${questions[questionIndex].question}</h2>`
	//console.log(title)

	headerContainer.innerHTML = title

	// варианты ответов
	for([index, answerText] of questions[questionIndex]['answers'].entries()){
		// console.log(index)
		// console.log(index+1, answerText)
		// метод entries вернет новый массив, который будет состоять из значений, в котором будет ключ и значение текущего массива
		// console.log(answerText)
		// запишем шаблон для ответов
		const answerHtml = `
		<li>
			<label>
					<input value="${index+1}" type="radio" class="answer" name="answer" /><!--в value будем вставлять номер ответа, который пользователь выбрал-->
					<span>${answerText}</span>
			</label>
		</li>
		`;
		// console.log(answerHtml)
		listContainer.innerHTML += answerHtml
	}

 }

 function checkAnswer(){
	const checkedRadio = listContainer.querySelector('input[type="radio"]:checked')
	// console.log(checkedRadio)
	 /*
	 if(checkedRadio){
		 console.log('OK')
	 }else{
		submitBtn.blur()
		return
	 }
	  */
	 if(!checkedRadio){
		 submitBtn.blur()
		 return
	 }
	 // console.log(checkedRadio.value)
	 const userAnswer = parseInt(checkedRadio.value) // узнаем номер ответа пользователя
	 // console.log(userAnswer)
	 
	 // получим номер правильного ответа для текущего вопроса
	 // console.log(questions[questionIndex]['correct'])
	 // console.log(userAnswer, questions[questionIndex]['correct'])
	 if(userAnswer === questions[questionIndex]['correct']){ // если ответ пользователя равен правильному ответу
		//
		score++ // увеличиваем балы пользователя на 1
		// console.log(score)
	 }
	 // Дальше мы должны проверить: это был последний вопрос?
	 if(questionIndex !== questions.length - 1){ // если это не последний вопрос
	 	// у данного массива 4 элемента, его длина - 4
		// индекс последнего элемента - 3
		// у последнего вопроса - индекс - 3
		// мы можем из длины массива вычесть 1 - получим 3
		// 3 - это как раз индекс последнего элемента
		// таким образом мы можем проверить: текущий индекс (questionIndex) - последний или нет?
		// questions.length - 4,   4-1=3, и если индекс последнего элемента 3 и он === 3, значит это последний вопрос
		// console.log('Это не последний вопрос')
		// увеличиваем индекс
		questionIndex++
		// далее очищаем страницу
		clearPage()
		// показываем следующий вопрос
		showQuestion() // функция отработает, запустит вопрос по индексу, а индекс мы уже увеличили
	 	return
	 }
	 else{
		 // console.log('Это последний вопрос')
		 // очищаем страницу
		 clearPage()
		 // показываем результаты
		 showResults()
	 }
	 
 }


 function showResults(){
	 // console.log('showResults started')
	 let title, message;

	 if(score === questions.length){ // если score ровна количеству вопросов, т.е. если он ответил на все вопросы правильно
		 title = 'Поздравляем! 😀'
		 message = 'Вы ответили на все вопросы! 🔥'
	 }else if((score * 100) / questions.length >=50 ){ // если мы ответили правильно более чем на половину вопросов
		// получим в процентном соотнешении от 0...100 на сколько процентов мы ответили верно
		// и если оно будет равно или больше 50, то мы ответили более на половину вопросов
		 title = 'Неплохой результат! 🙂'
		 message = 'Вы дали более половины правильных ответов! 👍'
	 }
	 else{
		 title = 'Стоит постараться 😐'
		 message = 'Пока у вас меньше половины правильных ответов 😔'
	 }

	 // Результат
	 let result = `${score} из ${questions.length}`


	 // шаблон с результатами
	 const resultTemplates = `
	 	<h2 class="title">${title}</h2>
		<h3 class="summary">${message}</h3>
		<p class="result">${result}</p>
	 `

	 // Выводим данные на страницу
	 headerContainer.innerHTML = resultTemplates

	 // обработка кнопки
	 submitBtn.blur() // снимем с кнопки активное состояние
	 submitBtn.innerText = 'Начать заново'
	 submitBtn.onclick = ()=>history.go()
	 // когда в history.go() не передаем никаких аргументов, то это равносильно обновлению страницы
 }

